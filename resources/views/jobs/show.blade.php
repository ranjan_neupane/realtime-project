@extends('layouts.app')

@section('content')
<style>
    .temp{
        background: red;
        pointer-events: none;
    }
</style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class ="card-header">{{$job->title}}</div>
                        <div class="card-body">
                            <p>
                                <h3>Description</h3>
                                {{$job->description}}
                            </p>
                            <p>
                                <h3>Duties</h3>
                                {{$job->roles}}
                            </p>
                        </div>
                </div>
                    @auth
                        <div class="form-group">
                            @php
                                $company_id = $job->company->user_id;
                                $job_id=$job->id;
                                $seeker_id=Auth::user()->id;
                            @endphp

                            <form action="{{route('jobs.apply')}}" method="post">
                            @csrf
                            <input type="hidden" name="company" value="{{$company_id}}">
                            <input type="hidden" name="seeker" value="{{$seeker_id}}">
                            <input type="hidden" name="job" value="{{$job_id}}">
                            @if ($applied == true)
                                <a href="" class ="btn btn-success">
                                    Already Applied
                                </a>
                                <a href="{{route('jobs.cancel')."?company=$company_id&seeker=$seeker_id&job=$job_id"}}" class="btn btn-danger">Cancel</a>
                            @else
                            <p>Bid for the Job</p>
                                <input type="number" value="" name="bid_value" required>
                                <button class="btn btn-success">Bid</button>

                            @endif
                            </form>
                        </div>
                    @else
                        <div class="form-group">
                            <a href="{{route('login')}}" class ="btn btn-success" >
                                Login To Apply
                            </a>
                        </div>
                    @endauth

                    <div class="col-md-4">
                        <div class="card">
                            <div class ="card-header">Short Info</div>
                                <div class="card-body">
                                    <p>Job Provider:<a href="{{route('company.index',[$job->company->id,$job->company->cname])}}">
                                    {{$job->company->cname}}</a></p>
                                    <p>Address: {{$job->address}}</p>
                                    <p>Job Position:{{$job->position}}</p>
                                    <p>Date:{{$job->last_date}}</p>
                                </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
